import React, { Component, Fragment } from 'react'
import {  Route, Redirect } from "react-router-dom";
import {PrivateRoute} from "./Components/PrivateRoute";
import Login from './Containers/Login'
import Home from './Containers/Home';

export default class App extends Component {
  render() {
    return (
      <Fragment>
            <Route exact path="/login" component={Login} />
            <PrivateRoute exact path="/home" component={Home}/>
            <Redirect from="/" to="/login" />
			</Fragment>
    )
  }
}
