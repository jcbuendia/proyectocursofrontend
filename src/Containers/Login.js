import React, { Component,Fragment } from 'react'
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import SendIcon from '@material-ui/icons/Send';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from "../Components/Inputs";
import auth from '../Constants/auth';
import axios from 'axios';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: "ejemplo@algo.com",
            password: "",
            imgb64: '',
            session: false,
            checkedA: true,
            codigo: false,
            open: false,
        }
    }
    componentDidMount = () => { 
    }
    componentDidUpdate =(prevState, prevProps) =>{
    }
    componentWillUnmount=()=>{
    }

    handlePeticion= async ()=>{
        try {
              const response = await axios.get('http://10.2.20.210:3000/authenticate-204');
              console.log(response);
              return  response;
            } catch (error) {
              console.error(error);
            }
          }
   
    handleClickButon = async () =>{
      if(this.state.password && this.state.email && this.state.imgb64){
       
       if(!/\S+@\S+\.\S+/.test(this.state.email)){
         alert("correo invalido");
         return;
       }

        const responde = await this.handlePeticion();
        const {data} = responde
        const {headerResponse, payload} = data
        // handle success
        
            switch (headerResponse.code){
                case 200:
                        const {token} = payload;
                    if (token){
                            auth.login(() => {
                                this.props.history.push('/home');
                                });
                    }
                    break;
                case 204:
                    console.log("Error 204 ");
                    this.handleClickOpen();
                    break;
                case 409:
                    console.log("Error 209");
                    break;
            }        
        

        

        
    }else{
        !this.state.email ? 
        alert("ingrese el email")
        :
        !this.state.password ?
        alert("ingrese el password ")
        :
        alert("ingrese el avatar ")
       }

    }

    handleChangeValues=(e)=>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleClickOpen = () => {
        this.setState({
          open: true,
        });
      };
    
      handleClose = () => {
        this.setState({ open: false });
      };

    handleChangeImage = (e) => {
        e.preventDefault();
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            this.setState({
                imgb64: reader.result
            });
        console.log(this.state.imgb64)
        };     
    }

    render() {
        return (
            <Fragment>
                <Dialog
                    onClose={this.handleClose}
                    aria-labelledby="customized-dialog-title"
                    open={this.state.open}
                    >
                </Dialog>
            <Grid
            container
            justify="center"
            direction="row"
            alignContent="center"
            alignItems="center"
            style={{ height: 600 , width: '100%', overflowY: 'hidden' }}
        >
                <Grid container 
                direction="row"
                justify="center"
                alignItems="center" 
                >
                <form autoComplete="off">
                <Paper style={{height: 325, width: 300, padding: 25}}>
                        <Grid container 
                        direction="column"
                        justify="center"
                        alignItems="center" 
                        spacing={40}
                        >
                            <Grid item xs={12}>
                                <Avatar alt="Remy Sharp" src={this.state.imgb64} style={{ margin: 10, width: 80, height: 80,}} />
                            </Grid> 
                            <Grid item xs={12}>
                                <Grid>
                                        <Input label="Ingresa email/usuario" name="email" onChangeData={this.handleChangeValues} value={this.state.email} type="text"/>
                                </Grid>
                                <Grid>
                                        <Input label="Ingresa password" name="password" onChangeData={this.handleChangeValues} value={this.state.password} type="password"/>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                            <input
                                    style={{margin: "auto"}}
                                        accept="image/*"
                                        style={{display: 'none'}}
                                        id="raised-button-file"
                                        multiple
                                        type="file"
                                        onChange={this.handleChangeImage}
                                    />
                                    <label htmlFor="raised-button-file">
                                        <Button raised component="span" color="primary" >
                                        Avatar
                                        </Button>
                                    </label>
                            </Grid> 
                            <Grid container style={{marginTop: 10}}>
                                <Grid xs={8}>
                                    <FormControlLabel
                                    control={
                                        <Checkbox
                                        checked={this.state.checkedA}
                                        name="checkedA"
                                        onChange={this.handleChangeValues}
                                        value="checkedA"
                                        />
                                    }
                                    label="Secondary"
                                    />
                                </Grid>
                                <Grid xs={4}>
                                        <Button variant="contained" style={{backgroundColor: "#49F249"}} type="reset" onClick={this.handleClickButon}>
                                        Go
                                        <SendIcon style={{marginLeft: 5}}></SendIcon>
                                        </Button>
                                </Grid>
                            </Grid>      
                        </Grid>
                    </Paper>    
                </form>
               </Grid>
               </Grid>
            </Fragment>
            )
        }
}
