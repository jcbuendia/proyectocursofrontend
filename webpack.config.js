const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); 
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
    entry: ['./src/index.js'].filter(Boolean),
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
      },
    module: {
        rules: [
            {
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
            },
            {
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			},
            {
				test: /\.(eot|ttf|woff|woff2|svg|otf)$/,
				use: [{ loader: 'file-loader' }],
			},
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({template: './public/index.html'}),
        new MiniCssExtractPlugin({
			filename: 'css/build.css'
		}),
        new webpack.HotModuleReplacementPlugin(),

    ] 
  };
